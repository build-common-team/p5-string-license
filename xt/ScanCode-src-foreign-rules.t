use Test2::V0;

use lib 't/lib';
use Test2::ScanCode;

plan 1;

are_licensed_like_scancode(
	[qw(src/licensedcode/data/non-english/rules)],
	'xt/ScanCode-src-foreign-rules.todo'
);

done_testing;
