use Test2::V0;

use lib 't/lib';
use Test2::ScanCode;

plan 7;

are_licensed_like_scancode(
	[qw(src/licensedcode/data/composites/rules)],
	'xt/ScanCode-src-composites-rules.todo'
);

done_testing;
