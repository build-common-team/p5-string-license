use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);

use Test2::V0;

use Path::Tiny;
use Test::Synopsis;

my $files = path('lib')->visit(
	sub ( $path, $state ) {
		$state->{$path}++ if $_->is_file and /\.pm\z/;
	},
	{ recurse => 1 }
);

synopsis_ok keys %$files;

done_testing;
